GHGSat Plume Challenge
======================

This is a project to solve the `challenges por Web Developer <challenges.md>`_

Deploy
------

Set environment variables
^^^^^^^^^^^^^^^^^^^^^^^^^

There's a sample file ``env.sample`` which you have to modify to set
the proper environment variables to run the project correctly and then
change the file name to ``.env``.

Most of them are common Django's project variables, but there's one
needed to get maps imagery from OpenStreemMaps, the variable is called
``MAPS_USER_KEY``, get a free key from https://mapquestapi.com/ and set
it there.


Install dependencies
^^^^^^^^^^^^^^^^^^^^
First of all, create a **virtual environment** for the project and
activate it.

There are 2 ``requirements`` files:

- ``requirements.txt`` which contains dependencies to have the project
  running on a server.
- ``requirements-dev.txt`` which contains dependencies to have the project
  running on a localhost as developer to add or modify the code.

You can install the dependencies with `pip` as any other project just by
running:

.. code-block:: bash

  pip install -U -r requirements.txt


Run the server
^^^^^^^^^^^^^^

Run the server in development mode with:

.. code-block:: bash

  python manage.py runserver

and open a browser to http://127.0.0.1:8000/ to use the web application
or http://127.0.0.1:8000/api/openapi/ to see an OpenAPI Schema to know
how to use the public API of the project.


Generate Sphinx documentation
-----------------------------

This project has a documentation made with Sphinx, to generate it go to
the ``docs/`` folder and run:

.. code-block:: bash

  make html

then open the file ``_build/html/index.html`` with a browser.
