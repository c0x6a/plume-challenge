Application API
===============

This API will allow you to get imagery from online satellite maps
and olverlay a plume on it.

The API has 2 endpoint you can use to interact with the project:

- Get satellite maps imagery:

  + URL: ``/api/v1/imagery/``
  + METHOD: `POST`
  + PAYLOAD:

  .. code-block:: json

    {
      "latitude": <latitude>,
      "longitude": <longitude>,
    }

  + RESULT:

  .. code-block:: json

    {
      "uuid": <uuid>,
      "imagery_b64": <base64_encoded_image>,
    }

- Upload a plume image to overlay on a map image:

  + URL: ``/api/v1/overlay/``
  + METHOD: `POST`
  + PAYLOAD:

  .. code-block:: json

    {
      "uuid": <uuid_of_coordinates>,
      "plume_file": <plume_file>,
    }

  + RESULT:

  .. code-block:: json

    {
      "plume_overlaid_file": <plume_overlaid_image_url>,
    }


There's an OpenAPI Schema available at:

``/api/openapi/``


.. toctree::
   :maxdepth: 2

   views
   serializers
