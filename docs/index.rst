.. eqhali_labs documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Project documentation
=====================

GHGSat Plume challenge

.. toctree::
   :maxdepth: 2


Content
-------

.. toctree::
   :maxdepth: 2

   api/index


Index
=====

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
