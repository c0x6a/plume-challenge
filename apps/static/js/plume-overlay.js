$(function () {
  let $formCoordinates = $("#formCoordinates");
  let $formPlumeFile = $("#formPlumeFile");
  let $mapImagery = $("#mapImagery");
  let $imageryUUID = $("#imageryUUID");

  $formCoordinates.submit(function () {
    $mapImagery.prop("src", "");
    $.ajax({
      url   : $formCoordinates.attr("action"),
      method: $formCoordinates.attr("method"),
      data  : $formCoordinates.serialize(),
    }).done(function (data) {
      $mapImagery.prop("src", `data:image/png;base64,${data.imagery_b64}`);
      $imageryUUID.val(data.uuid);
    }).fail(function (data) {
      console.error(data.responseJSON);
    });

    return false;
  });


  $formPlumeFile.submit(function () {
    let formData = new FormData($formPlumeFile[0]);
    $.ajax({
      url        : $formPlumeFile.attr("action"),
      method     : $formPlumeFile.attr("method"),
      data       : formData,
      processData: false,
      contentType: false,
      cache      : false,
    }).done(function (data) {
      $mapImagery.prop("src", `${data.plume_overlaid_file}`);
    }).fail(function (data) {
      console.error(data.responseJSON);
    });

    return false;
  })
});
