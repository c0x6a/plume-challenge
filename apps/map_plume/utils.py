import base64
from io import BytesIO

from django.conf import settings

import requests

from PIL import Image


def imagery_from_coordinates(latitude, longitude):
    imagery_url = (
        f'{settings.OSM_MAPS_SERVER_URL}'
        f'map?key={settings.MAPS_USER_KEY}'
        f'&center={latitude},{longitude}'
        '&size=514,257'
        '&type=sat'
        '&zoom=12'
        '&scalebar=true'
    )
    imagery_content = requests.get(imagery_url).content
    imagery_b64 = base64.b64encode(imagery_content).decode()
    return imagery_b64


def merge_images(imagery, plume_img):
    imagery_img = Image.open(BytesIO(base64.b64decode(imagery.imagery_b64)))
    plume_b64 = base64.b64encode(plume_img.read())
    plume = Image.open(BytesIO(base64.b64decode(plume_b64)))
    imagery_img.paste(plume, (0, 0), plume)
    output = BytesIO()
    imagery_img.save(output, format='JPEG', quality=85)
    output.seek(0)
    return output
