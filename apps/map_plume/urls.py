from django.urls import path

from apps.map_plume.views import MapView

urlpatterns = [
    path('', MapView.as_view())
]
