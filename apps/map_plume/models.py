import uuid

from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models

from apps.map_plume.utils import imagery_from_coordinates


class Imagery(models.Model):
    """Model to store imagery from a given coordinate and the resulting
    plume overlaid on it"""
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
    )
    latitude = models.FloatField(
        validators=[
            MaxValueValidator(90),
            MinValueValidator(-90),
        ],
    )
    longitude = models.FloatField(
        validators=[
            MaxValueValidator(90),
            MinValueValidator(-90),
        ],
    )
    imagery_b64 = models.TextField(
        null=True,
    )
    plume_overlaid = models.ImageField(
        null=True,
        upload_to='overlaids'
    )

    class Meta:
        unique_together = ('latitude', 'longitude')

    def __str__(self):
        return str(self.uuid)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.imagery_b64 is None:
            self.imagery_b64 = imagery_from_coordinates(
                self.latitude,
                self.longitude,
            )
        super().save(force_insert, force_update, using, update_fields)
