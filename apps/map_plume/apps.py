from django.apps import AppConfig


class MapPlumeConfig(AppConfig):
    name = 'apps.map_plume'
