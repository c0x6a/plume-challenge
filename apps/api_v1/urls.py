from django.urls import include, path

app_name = 'api_v1'
urlpatterns = [
    path('', include('apps.api_v1.map_plume_api.urls', namespace='map-plume_img-api'))
]
