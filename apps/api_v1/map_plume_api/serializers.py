from rest_framework import serializers

from apps.map_plume.models import Imagery


class CoordinatesSerializer(serializers.ModelSerializer):
    """Coordinates serializer to validate proper input and output

    Input values:

    - `latitude`: float number from -90.0000000 to 90.0000000
    - `longitude`:  float number from -90.0000000 to 90.0000000

    Output values:

    - `uuid`: UUID of the stored object for the given coordinates
    - `imagery_b64`: Imagery from map in the given coordinates, encoded
      in `base64`
    """
    uuid = serializers.UUIDField(read_only=True)
    imagery_b64 = serializers.CharField(read_only=True)

    class Meta:
        model = Imagery
        fields = (
            'latitude',
            'longitude',
            'uuid',
            'imagery_b64',
        )


class PlumeFileSerializer(serializers.ModelSerializer):
    """Serializer to overlay a plume image on already saved imagery

    Input values:

    - `uuid`: UUID of the stored object for the given coordinates
    - `plume_file`: A plume file to overlay on the imagery
    """
    plume_file = serializers.ImageField()

    class Meta:
        model = Imagery
        fields = (
            'uuid',
            'plume_file',
        )
