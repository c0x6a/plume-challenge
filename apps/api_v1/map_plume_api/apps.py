from django.apps import AppConfig


class MapPlumeApiConfig(AppConfig):
    name = 'map_plume_api'
