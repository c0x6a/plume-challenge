from django.urls import path

from apps.api_v1.map_plume_api.views import GetImageryMapAPIView, OverlayPlumeAPIView

app_name = 'map_plume_api'

urlpatterns = [
    path('imagery/', GetImageryMapAPIView.as_view(), name='imagery'),
    path('overlay/', OverlayPlumeAPIView.as_view(), name='overlay'),
]
