import sys

from django.core.files.uploadedfile import InMemoryUploadedFile

from apps.api_v1.map_plume_api.serializers import CoordinatesSerializer, PlumeFileSerializer
from apps.map_plume.models import Imagery
from apps.map_plume.utils import merge_images
from rest_framework import status
from rest_framework.generics import CreateAPIView, GenericAPIView, get_object_or_404
from rest_framework.response import Response


class GetImageryMapAPIView(CreateAPIView):
    """Get a map imagery centered on given coordinates.

    **Input values:** (from an HTTP `post` request)

    - `latitude`: float number from -90.0000000 to 90.0000000
    - `longitude`:  float number from -90.0000000 to 90.0000000
    """
    serializer_class = CoordinatesSerializer

    def create(self, request, *args, **kwargs):
        try:
            imagery = Imagery.objects.get(
                latitude=request.data.get('latitude'),
                longitude=request.data.get('longitude'),
            )
            data = {
                'latitude': imagery.latitude,
                'longitude': imagery.longitude,
                'uuid': imagery.uuid,
                'imagery_b64': imagery.imagery_b64,
            }
            print(data)
            return Response(data, status.HTTP_200_OK)
        except Imagery.DoesNotExist:
            return super().create(request, *args, **kwargs)


class OverlayPlumeAPIView(GenericAPIView):
    """Overlay a plume on a imagery

    **Input values:** (from an HTTP `post` request)

    - `uuid`: UUID of the stored object for the given coordinates
    - `plume_file`: A plume file to overlay on the imagery
    """
    serializer_class = PlumeFileSerializer

    def post(self, request, *args, **kwargs):
        imagery = get_object_or_404(Imagery, uuid=self.request.data.get('uuid'))
        data = self.serializer_class(data=self.request.FILES)
        data.is_valid(raise_exception=True)
        plume = data.validated_data['plume_file']
        if not imagery.plume_overlaid:
            plume_overlaid = merge_images(imagery, plume)
            imagery.plume_overlaid = InMemoryUploadedFile(
                plume_overlaid,
                'plume_overlaid',
                f'overlaid.jpg',
                'image/jpeg',
                sys.getsizeof(plume_overlaid),
                None,
            )
            imagery.save()
        return Response({'plume_overlaid_file': imagery.plume_overlaid.url}, status.HTTP_200_OK)
