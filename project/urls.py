"""project URL Configuration"""

from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path

from rest_framework.schemas import get_schema_view

urlpatterns = [
    path('', include('apps.map_plume.urls')),
    path('api/v1/', include(('apps.api_v1.urls', 'apps.api_v1.urls'), namespace='api-v1')),
    path('api/openapi/', get_schema_view(
        title="Plume Overlay",
        description="API"
    ), name='openapi-schema'),
]
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
